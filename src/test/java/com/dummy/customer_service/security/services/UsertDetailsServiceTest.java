package com.dummy.customer_service.security.services;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.dummy.customer_service.entities.User;
import com.dummy.customer_service.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class UsertDetailsServiceTest {

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private UserDetailsImpl userDetailsImpl;

	@InjectMocks
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@Test
	public void getCustomersTest() {

		User user = new User();
		user.setId(1);
		user.setFullName("Test User");
		user.setUserName("username");
		user.setPassword("password");

		User user2 = new User(user.getFullName(), user.getUserName(), user.getPassword());

		Mockito.when(userRepository.findByUserName("dummy")).thenReturn(Optional.of(user));
		userDetailsServiceImpl.loadUserByUsername("dummy");

	}

}
