package com.dummy.customer_service.security.services;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityNotFoundException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.dummy.customer_service.entities.Address;
import com.dummy.customer_service.entities.Customer;
import com.dummy.customer_service.model.AddressDetailsVO;
import com.dummy.customer_service.model.CustomerVO;
import com.dummy.customer_service.repository.AddressRepository;
import com.dummy.customer_service.repository.CustomerRepository;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private AddressRepository addressRepository;

	@InjectMocks
	private CustomerServiceImpl customerServiceImpl;

	@Test
	public void getCustomersTest() {
		Address address = new Address();
		address.setState("UP");
		Set<Address> addresses = new HashSet<Address>();
		addresses.add(address);
		Customer customer = new Customer();
		customer.setCustomerId("1");
		customer.setFirstName("dummy");
		customer.setAddress(addresses);
		List<Customer> customers = new ArrayList<>();
		customers.add(customer);
		Page<Customer> pagedTasks = new PageImpl<>(customers);
		Mockito.when(customerRepository.findAll(org.mockito.ArgumentMatchers.isA(Pageable.class)))
				.thenReturn(pagedTasks);
		customerServiceImpl.getCustomers(1);

	}

	@Test
	public void getCustomerByIdTest() {
		AddressDetailsVO addressDetailsVO1 = new AddressDetailsVO();
		addressDetailsVO1.setState("UP");
		AddressDetailsVO addressDetailsVO2 = new AddressDetailsVO();
		addressDetailsVO2.setState("DL");
		List<AddressDetailsVO> addressDetailsVOs = new ArrayList<AddressDetailsVO>();
		addressDetailsVOs.add(addressDetailsVO1);
		addressDetailsVOs.add(addressDetailsVO2);
		CustomerVO customerVO = new CustomerVO();
		customerVO.setCustomerId("1");
		customerVO.setFirstName("dummy");
		customerVO.setAddressDetails(addressDetailsVOs);

		Address address = new Address();
		address.setState("UP");
		Set<Address> addresses = new HashSet<Address>();
		addresses.add(address);
		Customer customer = new Customer();
		customer.setCustomerId("1");
		customer.setFirstName("dummy");
		customer.setAddress(addresses);

		Mockito.when(customerRepository.findById("1")).thenReturn(Optional.of(customer));
		customerServiceImpl.getCustomerById("1");

	}

	@Test(expected = EntityNotFoundException.class)
	public void getCustomerByIdTestException() {
		Mockito.when(customerRepository.findById("1")).thenThrow(EntityNotFoundException.class);
		customerServiceImpl.getCustomerById("1");

	}

	@Test
	public void saveCustomerTest() {
		AddressDetailsVO addressDetailsVO1 = new AddressDetailsVO();
		addressDetailsVO1.setState("UP");
		List<AddressDetailsVO> addressDetailsVOs = new ArrayList<AddressDetailsVO>();
		addressDetailsVOs.add(addressDetailsVO1);
		CustomerVO customerVO = new CustomerVO();
		customerVO.setCustomerId("1");
		customerVO.setFirstName("dummy");
		customerVO.setAddressDetails(addressDetailsVOs);

		Address address = new Address();
		address.setState("UP");
		List<Address> addressesList = new ArrayList<>();
		addressesList.add(address);
		Set<Address> addresses = new HashSet<Address>();
		addresses.add(address);
		Customer customer = new Customer();
		customer.setCustomerId("1");
		customer.setFirstName("dummy");
		customer.setAddress(addresses);
		List<Customer> customers = new ArrayList<>();
		customers.add(customer);

		when(customerRepository.save(Mockito.any())).thenReturn(customer);
		customerServiceImpl.save(customerVO);
	}

}
