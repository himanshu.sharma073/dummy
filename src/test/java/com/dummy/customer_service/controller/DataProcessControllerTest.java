package com.dummy.customer_service.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dummy.customer_service.model.AddressDetailsVO;
import com.dummy.customer_service.model.CustomerVO;
import com.dummy.customer_service.security.jwt.AuthEntryPointJwt;
import com.dummy.customer_service.security.jwt.JwtUtils;
import com.dummy.customer_service.security.services.CustomerService;
import com.dummy.customer_service.security.services.UserDetailsServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(DataProcessController.class)
public class DataProcessControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private CustomerService customerService;

	@MockBean
	private UserDetailsService userDetailsService;

	@MockBean
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@MockBean
	private DataProcessController dataProcessController;

	@MockBean
	private AuthEntryPointJwt authEntryPointJwt;

	@MockBean
	private JwtUtils jwtUtils;

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext wac;

	@Before
	public void setup() {
		DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
		this.mockMvc = builder.build();
	}

	@Test
	public void ping() throws Exception {
		// dataProcessController.ping();
		// mvc.perform(get("/ping"));
		mvc.perform(MockMvcRequestBuilders.get("/ping"));
	}

	@Test
	public void getCustomersTest() throws Exception {
		AddressDetailsVO addressDetailsVO1 = new AddressDetailsVO();
		addressDetailsVO1.setState("UP");
		AddressDetailsVO addressDetailsVO2 = new AddressDetailsVO();
		addressDetailsVO2.setState("DL");
		List<AddressDetailsVO> addressDetailsVOs = new ArrayList<AddressDetailsVO>();
		addressDetailsVOs.add(addressDetailsVO1);
		addressDetailsVOs.add(addressDetailsVO2);
		CustomerVO customerVO = new CustomerVO();
		customerVO.setCustomerId("1");
		customerVO.setFirstName("dummy");
		customerVO.setAddressDetails(addressDetailsVOs);
		List<CustomerVO> customerVOs = new ArrayList<>();
		customerVOs.add(customerVO);
		Mockito.when(customerService.getCustomers(1)).thenReturn(customerVOs);
		mvc.perform(MockMvcRequestBuilders.get("/v1/customers")).andExpect(status().isOk());
	}

}
