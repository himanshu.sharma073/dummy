package com.dummy.customer_service.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dummy.customer_service.entities.User;
import com.dummy.customer_service.model.LoginRequest;
import com.dummy.customer_service.model.SignupRequest;
import com.dummy.customer_service.repository.UserRepository;
import com.dummy.customer_service.security.jwt.AuthEntryPointJwt;
import com.dummy.customer_service.security.jwt.JwtUtils;
import com.dummy.customer_service.security.services.UserDetailsServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(AuthController.class)
public class AuthControllerTest {

	@MockBean
	private UserRepository userRepository;

	@MockBean
	private JwtUtils jwtUtils;

	@MockBean
	private PasswordEncoder passwordEncoder;

	@MockBean
	private AuthenticationManager authenticationManager;

	@MockBean
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@MockBean
	private AuthController authController;

	@MockBean
	private AuthEntryPointJwt authEntryPointJwt;

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
		this.mockMvc = builder.build();
	}

	@Test
	public void authenticateUserTest() throws Exception {
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsername("user");
		loginRequest.setPassword("password");
		authController.authenticateUser(loginRequest);

		Mockito.when(authenticationManager.authenticate(Mockito.mock(UsernamePasswordAuthenticationToken.class)))
				.thenReturn(Mockito.mock(Authentication.class));

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/auth/signin")
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(loginRequest))
				.contentType(MediaType.APPLICATION_JSON);
		this.mockMvc.perform(builder);

	}

	private static String asJsonString(final Object obj) throws JsonProcessingException {
		final ObjectMapper mapper = new ObjectMapper();
		final String jsonContent = mapper.writeValueAsString(obj);
		return jsonContent;
	}

	@Test
	public void registerUserTest() throws Exception {
		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUserName("userName");
		signupRequest.setPassword("password");
		authController.registerUser(signupRequest);

		User user = new User("Test User", "username", "password");
		user.toString();

		Mockito.when(authenticationManager.authenticate(Mockito.mock(UsernamePasswordAuthenticationToken.class)))
				.thenReturn(Mockito.mock(Authentication.class));

		Mockito.when(userRepository.save(user)).thenReturn(user);

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/auth/signup")
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(signupRequest))
				.contentType(MediaType.APPLICATION_JSON);
		this.mockMvc.perform(builder);

	}

}