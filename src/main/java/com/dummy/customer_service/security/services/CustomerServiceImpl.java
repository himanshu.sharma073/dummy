package com.dummy.customer_service.security.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.dummy.customer_service.entities.Address;
import com.dummy.customer_service.entities.Customer;
import com.dummy.customer_service.model.AddressDetailsVO;
import com.dummy.customer_service.model.CustomerVO;
import com.dummy.customer_service.model.SaveCustomerResponse;
import com.dummy.customer_service.repository.AddressRepository;
import com.dummy.customer_service.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Override
	public List<CustomerVO> getCustomers(Integer limit) {
		return customerRepository.findAll(PageRequest.of(0, limit)).toList().parallelStream().map(this::convert)
				.collect(Collectors.toList());
	}

	@Override
	public CustomerVO getCustomerById(String customerId) {
		return convert(customerRepository.findById(customerId)
				.orElseThrow(() -> new EntityNotFoundException("Customer Not Found!")));
	}

	@Override
	public SaveCustomerResponse save(CustomerVO customerVO) {
		Customer customer = new Customer();
		BeanUtils.copyProperties(customerVO, customer);
		final Customer savedCustomer = customerRepository.save(customer);
		addressRepository.saveAll(customerVO.getAddressDetails().stream().map((avo) -> {
			Address address = new Address();
			BeanUtils.copyProperties(avo, address);
			address.setCustomer(savedCustomer);
			return address;
		}).collect(Collectors.toList()));
		return new SaveCustomerResponse(savedCustomer.getCustomerId());
	}

	private CustomerVO convert(Customer customer) {
		CustomerVO cvo = new CustomerVO();
		BeanUtils.copyProperties(customer, cvo);
		cvo.setAddressDetails(customer.getAddress().stream().map((addr) -> {
			AddressDetailsVO avo = new AddressDetailsVO();
			BeanUtils.copyProperties(addr, avo);
			return avo;
		}).collect(Collectors.toList()));
		return cvo;
	}

}
