package com.dummy.customer_service.security.services;

import java.util.List;

import com.dummy.customer_service.model.CustomerVO;
import com.dummy.customer_service.model.SaveCustomerResponse;

public interface CustomerService {

	List<CustomerVO> getCustomers(Integer limit);

	CustomerVO getCustomerById(String customerId);

	SaveCustomerResponse save(CustomerVO customer);

}
