package com.dummy.customer_service.model;

import java.util.ArrayList;
import java.util.List;

public class CustomerVO {
	
	private String customerId;
	
	private String firstName;
	
	private String lastName;
	
	private String emailId;
	
	private String phoneNumber;
	
	private List<AddressDetailsVO> addressDetails = new ArrayList<AddressDetailsVO>();

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<AddressDetailsVO> getAddressDetails() {
		return addressDetails;
	}

	public void setAddressDetails(List<AddressDetailsVO> addressDetails) {
		this.addressDetails = addressDetails;
	}
	
}
