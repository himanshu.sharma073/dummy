package com.dummy.customer_service.model;

public class JwtResponse {

	private String token;
	private int id;
	private String username;

	public JwtResponse(String token, int id, String username) {
		super();
		this.token = token;
		this.id = id;
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
