package com.dummy.customer_service.model;

public class SaveCustomerResponse {
	
	private String customerId;

	public SaveCustomerResponse(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}
