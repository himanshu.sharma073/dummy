package com.dummy.customer_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dummy.customer_service.entities.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String>  {

}
