package com.dummy.customer_service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dummy.customer_service.model.CustomerVO;
import com.dummy.customer_service.model.SaveCustomerResponse;
import com.dummy.customer_service.security.services.CustomerService;

@CrossOrigin(origins = "*")
@RestController
public class DataProcessController {
	
	@Autowired
	private CustomerService service;
	
	@GetMapping("/ping")
	public String ping() {
		return "pong";
	}
	
	@GetMapping("/v1/customers")
	public List<CustomerVO> getCustomers(Integer limit) {
		return service.getCustomers(limit);
	}
	
	@GetMapping("/v1/customers/{customerId}")
	public CustomerVO getCustomerById(@PathVariable String customerId) {
		return service.getCustomerById(customerId);
	}
	
	@PostMapping("/customers")
	public SaveCustomerResponse save(@RequestBody CustomerVO customer) {
		return service.save(customer);
	}
}
